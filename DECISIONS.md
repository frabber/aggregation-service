# FEDEX ASSIGNMENT

## Assumptions
Since in my opinion there was some room for interpretation here are my assumptions:
By backend requests I mean calls to the underlying individual rest services.
- Requests are combined and started in order of arrival. But still backend requests can run in parallel due to their  
  asynchronous/non blocking start in my implementation.
- Single queries with more than 5 values for the query parameter per shipment/pricing or track are discarded
- Any requests that arrived longer than 5 seconds ago are executed asap by timeout expiry.
- Current requests are executed if the sum of the next and current request sizes is larger than 5. No need to wait,
  since adding the next request will never amounts to 5 or less.
- Duplicate queries in the request are deduplicated. The deduplicated combined count is used to trigger the threshold. 
  Eg track=NL,DE and track=NL,GB total count is 3, not 4.

## Implementation choices
- I dabbled into quarkus to see what the hype is all about and to make this assignment worth my while. 
  Normally I would use spring-boot/resttemplate/http connection pooling and java.util.concurrent for the most part.
  But the world is moving on and more into reactive streams.
- Hence I'll be using quarkus reactive streams framework (Mutiny) with asynchronous non blocking calls
  (for the most part and only one direct use of java.util.concurrent).
- Use of one blockingqueue type to sequentially aggregate requests because the business logic in the assignment calls for this.
- Beware the quarkus default back pressure and retry mechanics might alter the expected behaviour of any external
  automated tests that rely on different mechanics. I have set retry to 0 where possible.
- Many more unit/integration tests to be written before considered production ready. 
- Some profiling work could be beneficial.
- Some refactoring to make the code more generic could be beneficial (both test and non test code). Possibly writing a little test framework.

## Unit test
- The AggregationResourceBulkTest runs sets of 1 - 5 calls in async and non blocking. A set has 5 requests for each api and hence 
  does not need to wait for processing. It also logs the same requests in curl format for testing outside of the unit test.
  You can set the AggregationResourceBulkTest.NUM_PARALLEL_TESTS to a higher number (eg 5000), but then you probably want ``quarkus.log.category."nl.
  interfixus.aggregation.resource".level=INFO``
- The AggregationResourceTest contains some logic tests. I set the timeout ``aggregation.timeout=1000`` to 1 second in this case
- RequestProcessorTest could benefit from more tests, but I covered it through the other two tests. Because in this case
  (and the way I approached it) the integration tests gave me more confidence.
- Code coverage reports can be found in aggregation-service\target\jacoco-report