# aggregation-service 

## Build and run
Backend base url can be configured in application.properties: 
```
quarkus.rest-client.individual-api.url
```
Run with:
```shell script
./mvnw package
java -jar target/quarkus-app/quarkus-run.jar
```

Quick parallel smoketest:
```
curl --parallel  --parallel-immediate  --parallel-max 50  "localhost:8080/aggregation?pricing=[0-100]"
```

The AggregationResourceBulkTest outputs the same calls in curl format such that they can be used against a running app.

See [DECISIONS.md](DECISIONS.md) for extra info regarding assumptions, rationale etc.

## Below is generated doc (usefull to try different running scenarios, eg native)
This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

## Packaging and running the application

The application can be packaged using:
```shell script
./mvnw package
```
It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

If you want to build an _über-jar_, execute the following command:
```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application, packaged as an _über-jar_, is now runnable using `java -jar target/*-runner.jar`.

## Creating a native executable

You can create a native executable using: 
```shell script
./mvnw package -Pnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: 
```shell script
./mvnw package -Pnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/aggregation-service-1.0.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/maven-tooling.

