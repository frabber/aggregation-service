/*
 * Copyright (c) 2022, Faizal Abdoelrahman, Interfixus.
 */

package nl.interfixus.aggregation.client;

import java.util.Map;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import org.eclipse.microprofile.faulttolerance.Retry;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import io.smallrye.mutiny.Uni;

@Path("/track")
@RegisterRestClient(configKey = "individual-api")
public interface TrackClient extends RestClient {

    @GET
    @Retry(maxRetries = 0)
    @Override
    Uni<Map<String, Object>> get(@QueryParam("q") Set<String> query);

}
