package nl.interfixus.aggregation.client;

import java.util.Map;
import java.util.Set;

import javax.ws.rs.QueryParam;

import io.smallrye.mutiny.Uni;

public interface RestClient {
    Uni<Map<String, Object>> get(@QueryParam("q") Set<String> query);
}
