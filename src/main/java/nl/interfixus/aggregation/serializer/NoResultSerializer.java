/*
 * Copyright (c) 2022, Faizal Abdoelrahman, Interfixus.
 */

package nl.interfixus.aggregation.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import nl.interfixus.aggregation.model.ErrorResult;

public class NoResultSerializer extends StdSerializer<ErrorResult> {

    public NoResultSerializer() {
        this(null);
    }

    public NoResultSerializer(Class<ErrorResult> clazz) {
        super(clazz);
    }

    @Override
    public void serialize(ErrorResult value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeNull();
    }
}
