/*
 * Copyright (c) 2022, Faizal Abdoelrahman, Interfixus.
 */

package nl.interfixus.aggregation.model;

import java.util.Map;

public class Pair<KEY, VALUE> {
    private final KEY key;
    private final VALUE value;

    private Pair(KEY key, VALUE value) {
        this.key = key;
        this.value = value;
    }

    public static <K, V> Pair of(K key, V value) {
        return new Pair(key, value);
    }

    public static <K, V> Pair of(Map.Entry entry) {
        return new Pair(entry.getKey(), entry.getValue());
    }

    public KEY getKey() {
        return key;
    }

    public VALUE getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Pair{" +
            "key=" + key +
            ", value=" + value +
            '}';
    }
}
