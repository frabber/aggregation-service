/*
 * Copyright (c) 2022, Faizal Abdoelrahman, Interfixus.
 */

package nl.interfixus.aggregation.model;

public enum API {
    pricing,
    track,
    shipments
}
