/*
 * Copyright (c) 2022, Faizal Abdoelrahman, Interfixus.
 */

package nl.interfixus.aggregation.model;

import java.util.Collections;
import java.util.Set;

public class Request {
    final Set<String> query;

    final long timestamp;

    public Request(Set<String> query, long timestamp) {
        this.query = Collections.unmodifiableSet(query);
        this.timestamp = timestamp;
    }

    public int getSize() {
        return query.size();
    }

    public Set<String> getQuery() {
        return query;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return "Request{" +
            "query=" + query +
            ", timestamp=" + timestamp +
            '}';
    }
}
