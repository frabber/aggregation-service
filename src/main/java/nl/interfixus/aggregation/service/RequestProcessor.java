/*
 * Copyright (c) 2022, Faizal Abdoelrahman, Interfixus.
 */

package nl.interfixus.aggregation.service;

import static java.util.concurrent.TimeUnit.NANOSECONDS;
import static nl.interfixus.aggregation.model.ErrorResult.NONE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

import org.jboss.logging.Logger;

import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.operators.multi.processors.UnicastProcessor;
import nl.interfixus.aggregation.client.RestClient;
import nl.interfixus.aggregation.model.Pair;
import nl.interfixus.aggregation.model.Request;

public class RequestProcessor implements Runnable {
    private static final Logger LOG = Logger.getLogger(RequestProcessor.class);

    private final BlockingQueue<Request> incomingRequests;
    private final RestClient client;
    private final Map<Request, UnicastProcessor<Pair<String, Object>>> unicastProcessorMap = new ConcurrentHashMap<>();
    private volatile boolean cancelled = false;
    private final long aggregationTimeout; // nanoseconds
    public final int queryBatchSize;

    public RequestProcessor(RestClient client, long aggregationTimeout, int queryBatchSize) {
        this.client = client;
        this.aggregationTimeout = aggregationTimeout;
        this.queryBatchSize = queryBatchSize;
        this.incomingRequests = new LinkedBlockingQueue<>();
    }

    RequestProcessor(RestClient client, long aggregationTimeout, int queryBatchSize,
        BlockingQueue<Request> requestQueue) {
        this.client = client;
        this.aggregationTimeout = aggregationTimeout;
        this.queryBatchSize = queryBatchSize;
        this.incomingRequests = requestQueue;
    }

    public void cancel() {
        cancelled = true;
    }

    @Override
    public void run() {
        LOG.info("Starting RequestConsumer)");
        LinkedList<Request> pendingRequests = new LinkedList<>();

        while (!cancelled) {
            try {
                long timeout = getTimeout(pendingRequests);
                int totalQuerySize = getTotalQuerySize(pendingRequests);
                LOG.debug("polling queue, using timeout " + timeout / 1_000_000_000.0);
                Request request = incomingRequests.poll(timeout, NANOSECONDS);
                if (request != null) {
                    if (totalQuerySize + request.getSize() > queryBatchSize) {
                        transferRequests(pendingRequests);
                        conditionallyAddRequest(pendingRequests, request);
                    } else if (totalQuerySize + request.getSize() == queryBatchSize) {
                        pendingRequests.add(request);
                        transferRequests(pendingRequests);
                    } else { // request.getSize() < QUERY_BATCH_SIZE
                        pendingRequests.add(request);
                    }
                } else if (totalQuerySize > 0) { // request == null, timeout reached
                    LOG.debug("timeout reached");
                    transferRequests(pendingRequests);
                }
            } catch (InterruptedException e) {
                LOG.info("Interrupted, closing thread");
                Thread.currentThread().interrupt();
                cancel();
            } catch (RuntimeException e) {
                LOG.error("Error occurred", e);
            }
        }
        LOG.info("Exiting RequestConsumer)");
    }

    public Uni<Map<String, Object>> submitRequest(Set<String> query) {
        UnicastProcessor<Pair<String, Object>> unicastProcessor = UnicastProcessor.create();
        Uni<Map<String, Object>> result = unicastProcessor
            .collect()
            .asMap(Pair::getKey, Pair::getValue);
        Request request = new Request(query, System.nanoTime());
        unicastProcessorMap.put(request, unicastProcessor);
        addRequestToQueue(request);
        return result;
    }

    void addRequestToQueue(Request request) {
        try {
            incomingRequests.put(request);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException();
        }
    }

    void conditionallyAddRequest(List<Request> requests, Request request) {
        // discard requests with size > 5
        if (request.getSize() <= queryBatchSize) {
            requests.add(request);
        }
    }

    int getTotalQuerySize(List<Request> requests, Request request) {
        List<Request> combined = new ArrayList<>(requests);
        combined.add(request);
        return getTotalQuerySize(combined);
    }

    int getTotalQuerySize(List<Request> requests) {
        return requests.stream()
            .map(Request::getQuery)
            .flatMap(Set::stream)
            .collect(Collectors.toSet())
            .size();
    }

    private long getTimeout(LinkedList<Request> requests) {
        long timeout = Long.MAX_VALUE;
        if (!requests.isEmpty()) {
            long last = requests.getFirst().getTimestamp(); // LIFO stack
            long now = System.nanoTime();
            long elapsed = now - last;
            timeout = elapsed > aggregationTimeout ? 0 : aggregationTimeout - elapsed;
            LOG.debug(String.format("now %d last %d elapsed %d, timeout %d", now, last, elapsed, timeout));
        }
        return timeout;
    }

    private void transferRequests(List<Request> requests) {
        LOG.debug("Executing requests " + requests);
        List<Request> copiedRequests = new ArrayList<>(requests);
        aggregateRequestsAndExecute(copiedRequests)
            .subscribe()
            .with(item -> distributeResponse(copiedRequests, item));
        requests.clear();
    }

    private void distributeResponse(List<Request> requests, Map<String, Object> response) {
        requests.forEach(
            request -> {
                UnicastProcessor unicastProcessor = unicastProcessorMap.get(request);
                LOG.debug("Distribute response for request" + request);
                response.entrySet().stream()
                    .filter(entry -> request.getQuery().contains(entry.getKey()))
                    .forEach(entry -> {
                        unicastProcessor.onNext(Pair.of(entry));
                    });
                unicastProcessor.onComplete();
                unicastProcessorMap.remove(request);
            });
    }

    private Uni<Map<String, Object>> aggregateRequestsAndExecute(List<Request> requests) {
        Set<String> args = requests.stream()
            .map(Request::getQuery)
            .flatMap(Set::stream)
            .collect(Collectors.toSet());
        return getClientUni(args);
    }

    private Uni<Map<String, Object>> getClientUni(Set<String> query) {
        return client.get(query)
            //            .log()
            .onFailure()
            .recoverWithItem(() -> getEntry(query));
    }

    private Map<String, Object> getEntry(Set<String> query) {
        Map<String, Object> result = new HashMap<>();
        query.forEach(queryElement -> result.put(queryElement, NONE));
        return result;
    }

}
