/*
 * Copyright (c) 2022, Faizal Abdoelrahman, Interfixus.
 */

package nl.interfixus.aggregation.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import io.smallrye.mutiny.Uni;
import nl.interfixus.aggregation.model.API;
import nl.interfixus.aggregation.model.Pair;

@ApplicationScoped
public class AggregationService {

    @Inject
    BatchRequestService batchRequestService;

    public Uni<Map<API, Map<String, Object>>> aggregate(Set<String> pricingQuery, Set<String> trackQuery,
        Set<String> shipmentQuery) {
        List<Uni<Pair<API, Map<String, Object>>>> unis = new ArrayList(3);

        if (notEmpty(pricingQuery)) {
            unis.add(getClientUni(API.pricing, batchRequestService::getPricing, pricingQuery));
        }
        if (notEmpty(trackQuery)) {
            unis.add(getClientUni(API.track, batchRequestService::getTrack, trackQuery));
        }
        if (notEmpty(shipmentQuery)) {
            unis.add(getClientUni(API.shipments, batchRequestService::getShipment, shipmentQuery));
        }
        return Uni.combine().all().unis(unis)
            .combinedWith(mapEntries ->
                ((Stream<Pair<API, Map<String, Object>>>) mapEntries.stream())
                    .collect(Collectors.toMap(Pair::getKey, Pair::getValue))
            );
    }

    private Uni<Pair<API, Map<String, Object>>> getClientUni(API api,
        Function<Set<String>, Uni<Map<String, Object>>> client, Set<String> query) {
        return client.apply(query)
            .onItem()
            .transform(map -> Pair.of(api, map));
    }

    private static <T> boolean notEmpty(Set<T> set) {
        return set != null && !set.isEmpty();
    }
}
