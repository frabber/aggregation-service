/*
 * Copyright (c) 2022, Faizal Abdoelrahman, Interfixus.
 */

package nl.interfixus.aggregation.service;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.logging.Logger;

import io.smallrye.mutiny.Uni;
import nl.interfixus.aggregation.client.PricingClient;
import nl.interfixus.aggregation.client.ShipmentClient;
import nl.interfixus.aggregation.client.TrackClient;

@ApplicationScoped
public class BatchRequestService {
    private static final Logger LOG = Logger.getLogger(RequestProcessor.class);

    @RestClient
    PricingClient pricingClient;

    @RestClient
    TrackClient trackClient;

    @RestClient
    ShipmentClient shipmentClient;

    @ConfigProperty(name = "aggregation.timeout") // milliseconds
    long aggregationTimeout;

    @ConfigProperty(name = "aggregation.batchSize")
    int queryBatchSize;

    private final Executor executor = Executors.newFixedThreadPool(3);

    RequestProcessor pricingConsumer;
    RequestProcessor trackConsumer;
    RequestProcessor shipmentConsumer;

    @PostConstruct
    public void setup() {
        LOG.info("Setting up requestprocessor");
        aggregationTimeout = aggregationTimeout * 1_000_000;
        pricingConsumer = new RequestProcessor(pricingClient, aggregationTimeout, queryBatchSize);
        trackConsumer = new RequestProcessor(trackClient, aggregationTimeout, queryBatchSize);
        shipmentConsumer = new RequestProcessor(shipmentClient, aggregationTimeout, queryBatchSize);

        executor.execute(pricingConsumer);
        executor.execute(trackConsumer);
        executor.execute(shipmentConsumer);
    }

    public Uni<Map<String, Object>> getPricing(Set<String> query) {
        return pricingConsumer.submitRequest(query);
    }

    public Uni<Map<String, Object>> getTrack(Set<String> query) {
        return trackConsumer.submitRequest(query);
    }

    public Uni<Map<String, Object>> getShipment(Set<String> query) {
        return shipmentConsumer.submitRequest(query);
    }

}
