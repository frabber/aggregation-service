/*
 * Copyright (c) 2022, Faizal Abdoelrahman, Interfixus.
 */

package nl.interfixus.aggregation.resource;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import io.smallrye.mutiny.Uni;
import nl.interfixus.aggregation.model.API;
import nl.interfixus.aggregation.service.AggregationService;

@Path("/aggregation")
public class AggregationResource {

    @Inject
    AggregationService aggregationService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Map<API, Map<String, Object>>> aggregation(@QueryParam("pricing") String pricing,
        @QueryParam("track") String track,
        @QueryParam("shipments") String shipments) {
        // it seems in quarkus we have to split the string request parameters ourselves
        // instead of directly mapping to Set<String> request parameter
        return aggregationService.aggregate(asSet(pricing), asSet(track), asSet(shipments));
    }

    private Set<String> asSet(String request) {
        if (request == null || request.isEmpty()) {
            return Collections.emptySet();
        }
        return Arrays.stream(request.split(","))
            .map(String::trim)
            .collect(Collectors.toSet());
    }
}