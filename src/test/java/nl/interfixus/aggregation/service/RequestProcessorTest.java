/*
 * Copyright (c) 2022, Faizal Abdoelrahman, Interfixus.
 */

package nl.interfixus.aggregation.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import nl.interfixus.aggregation.client.PricingClient;
import nl.interfixus.aggregation.model.Request;

@QuarkusTest
class RequestProcessorTest {

    @InjectMock
    @RestClient
    PricingClient pricingClient;

    @Test
    public void combinedRequestSizes() {
        RequestProcessor requestProcessor = new RequestProcessor(null, 0, 0);
        Request r1 = new Request(Set.of("NL", "BE"), 0);
        Request r2 = new Request(Set.of("NL", "DE"), 0);
        List<Request> requests = List.of(r1, r2);
        int size = requestProcessor.getTotalQuerySize(requests);
        assertEquals(3, size);

        Request r3 = new Request(Set.of("NL", "DE", "GB"), 0);
        assertEquals(4, requestProcessor.getTotalQuerySize(requests, r3));
    }

    @Test
    public void queuePollingInterruptedExitThread() throws InterruptedException {
        BlockingQueue<Request> requestQueue = mock(BlockingQueue.class);
        RequestProcessor requestProcessor = new RequestProcessor(pricingClient, 5 * 1_000_000_000L,
            5, requestQueue);
        when(requestQueue.poll(anyLong(), any(TimeUnit.class))).thenThrow(InterruptedException.class);
        Assertions.assertDoesNotThrow(() -> requestProcessor.run());
    }

    @Test
    public void skipRequestWithMoreThanFiveRequestParameters() throws InterruptedException {
        BlockingQueue<Request> requestQueue = new LinkedBlockingQueue<>();
        Set<String> args = Set.of("NL", "BE", "DE", "IT", "UK", "PL");
        Request request = new Request(args, System.nanoTime());
        RequestProcessor requestProcessor = new RequestProcessor(pricingClient, 5 * 1_000_000_000L,
            5, requestQueue);
        requestProcessor.conditionallyAddRequest(Collections.emptyList(), request);
        assertTrue(requestQueue.isEmpty());
    }

    @Test
    public void addRequestToQueueThrowsInterrupted() throws InterruptedException {
        BlockingQueue<Request> requestQueue = mock(BlockingQueue.class);
        Set<String> args = Set.of("NL", "BE", "DE", "IT", "UK");
        Request request = new Request(args, System.nanoTime());
        RequestProcessor requestProcessor = new RequestProcessor(pricingClient, 5 * 1_000_000_000L,
            5, requestQueue);
        doThrow(new InterruptedException()).when(requestQueue).put(request);
        Assertions.assertThrows(RuntimeException.class, () -> requestProcessor.addRequestToQueue(request));
    }

}