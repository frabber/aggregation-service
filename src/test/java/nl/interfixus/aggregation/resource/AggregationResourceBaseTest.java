package nl.interfixus.aggregation.resource;

import static nl.interfixus.aggregation.model.API.pricing;
import static nl.interfixus.aggregation.model.API.shipments;
import static nl.interfixus.aggregation.model.API.track;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import io.smallrye.mutiny.helpers.test.UniAssertSubscriber;
import nl.interfixus.aggregation.model.Pair;

public class AggregationResourceBaseTest {

    Map<String, Object> getMockPricing(Set<String> callArgs) {
        return callArgs.stream()
            .map(arg -> (Pair<String, Double>)
                Pair.of(arg, (double) new Random().nextDouble()))
            .collect(Collectors.toMap(Pair::getKey, Pair::getValue));
    }

    Map<String, Object> getMockTrack(Set<String> callArgs) {
        String[] status = { "NEW", "IN TRANSIT", "COLLECTING", "COLLECTED", "DELIVERING", "DELIVERED" };
        return callArgs.stream()
            .map(arg -> (Pair<String, String>) Pair.of(arg, status[new Random().nextInt(status.length)]))
            .collect(Collectors.toMap(Pair::getKey, Pair::getValue));
    }

    Map<String, Object> getMockShipment(Set<String> callArgs) {
        String[] packageType = { "envelope", "box", "pallet" };
        return callArgs.stream()
            .map(arg -> (Pair<String, List<String>>) Pair.of(arg,
                IntStream.range(0, new Random().nextInt(11))
                    .boxed()
                    .map(i -> packageType[new Random().nextInt(packageType.length)])
                    .collect(Collectors.toList())))
            .collect(Collectors.toMap(Pair::getKey, Pair::getValue));
    }

    List<Set<String>> getListOfSetOfOrderNumbers(List<Integer> argCounts) {
        return argCounts.stream()
            .map(i -> new Random().ints(i, 10_000_000, 100_000_000)
                .boxed()
                .map(Object::toString)
                .collect(Collectors.toSet()))
            .collect(Collectors.toList());
    }

    List<Set<String>> getListOfSetOfCountries(List<Integer> argCounts) {
        return argCounts.stream()
            .map(i -> IntStream.range(0, i)
                .boxed()
                .map(j -> getRandomCountry())
                .collect(Collectors.toSet()))
            .collect(Collectors.toList());
    }

    String getRandomCountry() {
        return new Random().ints(65, 91)
            .limit(10)
            .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
            .toString();
    }

    Map<String, Map<String, Object>> getExpectedMap(Call pricingCall, Call trackCall, Call shipmentCall) {
        Map<String, Map<String, Object>> expectedMap = new HashMap<>();
        if (pricingCall != null) {
            expectedMap.put(pricing.name(), pricingCall.getExpected());
        }
        if (trackCall != null) {
            expectedMap.put(track.name(), trackCall.getExpected());
        }
        if (shipmentCall != null) {
            expectedMap.put(shipments.name(), shipmentCall.getExpected());
        }
        return expectedMap;
    }

    protected void assertSubscribers(List<Pair<UniAssertSubscriber, Map<String, Map<String, Object>>>> subscribers) {
        for (Pair<UniAssertSubscriber, Map<String, Map<String, Object>>> result : subscribers) {
            UniAssertSubscriber subscriber = result.getKey();
            Map<String, Map<String, Object>> expectedMap = result.getValue();
            subscriber
                .awaitItem()
                //                .awaitItem(Duration.of(5, ChronoUnit.MINUTES)) // when debugging
                .assertCompleted()
                .assertItem(expectedMap);
        }
    }
}
