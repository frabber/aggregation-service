package nl.interfixus.aggregation.resource;

import static java.lang.String.format;
import static java.util.List.of;
import static nl.interfixus.aggregation.model.API.pricing;
import static nl.interfixus.aggregation.model.API.shipments;
import static nl.interfixus.aggregation.model.API.track;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.logging.Logger;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.helpers.test.UniAssertSubscriber;
import nl.interfixus.aggregation.client.PricingClient;
import nl.interfixus.aggregation.client.ShipmentClient;
import nl.interfixus.aggregation.client.TrackClient;
import nl.interfixus.aggregation.model.API;
import nl.interfixus.aggregation.model.Pair;

@QuarkusTest
public class AggregationResourceBulkTest extends AggregationResourceBaseTest {

    private static final Logger LOG = Logger.getLogger(AggregationResourceBulkTest.class);
    public static int NUM_PARALLEL_TESTS = 30;

    @InjectMock
    @RestClient
    PricingClient pricingClient;

    @InjectMock
    @RestClient
    TrackClient trackClient;

    @InjectMock
    @RestClient
    ShipmentClient shipmentClient;

    @RestClient
    AggregationClient aggregationClient;

    List<List<List<Integer>>> callDistribution = of(
        of(of(5)),
        of(of(1, 4), of(4, 1), of(2, 3), of(3, 2)),
        of(of(1, 1, 3), of(1, 3, 1), of(3, 1, 1), of(1, 2, 2), of(2, 1, 2), of(2, 2, 1)),
        of(of(1, 1, 1, 2), of(1, 1, 2, 1), of(1, 2, 1, 1), of(2, 1, 1, 1)),
        of(of(1, 1, 1, 1, 1)));

    @Test
    void runNonblockingRequestsDistributeCallsInSetsOfFiveTotalRequestParameters() {
        List<Pair<UniAssertSubscriber, Map<String, Map<String, Object>>>> subscribers = new ArrayList<>();
        for (int i = 0; i < NUM_PARALLEL_TESTS; i++) {
            int distrCallIndex = i % 5;
            LOG.debug(format("%d executing set of %s", i + 1, distrCallIndex + 1));
            executeCalls(distrCallIndex, subscribers);
            if (i % 1000 == 0) {
                LOG.info(format("executed %d test", i));
            }
        }
        verify(pricingClient, times(NUM_PARALLEL_TESTS)).get(anySet());
        verify(trackClient, times(NUM_PARALLEL_TESTS)).get(anySet());
        verify(shipmentClient, times(NUM_PARALLEL_TESTS)).get(anySet());
    }

    private void executeCalls(int distrCallIndex,
        List<Pair<UniAssertSubscriber, Map<String, Map<String, Object>>>> subscribers) {
        List<Call> pricingCalls = query(pricing, distrCallIndex, this::getListOfSetOfCountries, this::getMockPricing);
        List<Call> trackCalls = query(track, distrCallIndex, this::getListOfSetOfOrderNumbers, this::getMockTrack);
        List<Call> shipmentCalls = query(shipments, distrCallIndex, this::getListOfSetOfOrderNumbers,
            this::getMockShipment);

        Set<String> combinedArgs = combineArgs(pricingCalls);
        Map<String, Object> mockPricingMap = combineExpected(pricingCalls);
        String message = "unharmful rare test collision (failure in generating unique test data), please rerun test";
        assertEquals(combinedArgs.size(), 5, message);
        assertEquals(mockPricingMap.size(), 5, message);
        when(pricingClient.get(combinedArgs)).thenReturn(Uni.createFrom().item(() -> mockPricingMap));

        combinedArgs = combineArgs(trackCalls);
        Map<String, Object> mockTrackMap = combineExpected(trackCalls);
        assertEquals(combinedArgs.size(), 5, message);
        assertEquals(mockTrackMap.size(), 5, message);
        when(trackClient.get(combinedArgs)).thenReturn(Uni.createFrom().item(() -> mockTrackMap));

        combinedArgs = combineArgs(shipmentCalls);
        Map<String, Object> mockShipmentMap = combineExpected(shipmentCalls);
        assertEquals(combinedArgs.size(), 5, message);
        assertEquals(mockShipmentMap.size(), 5, message);
        when(shipmentClient.get(combinedArgs)).thenReturn(Uni.createFrom().item(() -> mockShipmentMap));

        for (int callIndex = 0; callIndex <= distrCallIndex; callIndex++) {

            Call pricingCall = pricingCalls.get(callIndex);
            Call trackCall = trackCalls.get(callIndex);
            Call shipmentCall = shipmentCalls.get(callIndex);

            Map<String, Map<String, Object>> expectedMap = getExpectedMap(pricingCall, trackCall,
                shipmentCall);
            String request = format("/aggregation?%s&%s&%s", pricingCall.getQuery(), trackCall.getQuery(),
                shipmentCall.getQuery());
            LOG.debug(format("\tcurl \"localhost:8080%s\" &", request)); // useful to verify with curl against app
            UniAssertSubscriber<Map<String, Map<String, Object>>> subscriber =
                aggregationClient.get(pricingCall.getArgsAsQueryString(),
                        trackCall.getArgsAsQueryString(),
                        shipmentCall.getArgsAsQueryString())
                    //                    .log()
                    .subscribe()
                    .withSubscriber(UniAssertSubscriber.create());
            subscribers.add(Pair.of(subscriber, expectedMap));
        }

        assertSubscribers(subscribers);
    }

    private Map<String, Object> combineExpected(List<Call> pricingCalls) {
        Map<String, Object> expectedMap = pricingCalls.stream()
            .map(Call::getExpected)
            .map(m -> m.entrySet())
            .flatMap(Collection::stream)
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        return expectedMap;
    }

    private Set<String> combineArgs(List<Call> pricingCalls) {
        Set<String> combinedArgs = pricingCalls.stream()
            .map(Call::getArgs)
            .flatMap(Collection::stream)
            .collect(Collectors.toSet());
        return combinedArgs;
    }

    private List<Call> query(API api, int distrCallIndex, Function<List<Integer>, List<Set<String>>> generateQueryArgs,
        Function<Set<String>, Map<String, Object>> expectedGenerator) {
        Random rand = new Random();
        List<List<Integer>> callDistribution = this.callDistribution.get(distrCallIndex);
        List<Integer> argCounts = callDistribution.get(rand.nextInt(callDistribution.size()));
        List<Set<String>> argsInCalls = generateQueryArgs.apply(argCounts);
        return argsInCalls.stream()
            .map(callArgs -> {
                Map<String, Object> expected = expectedGenerator.apply(callArgs);
                return new Call(api, callArgs, expected);
            })
            .collect(Collectors.toList());
    }

}