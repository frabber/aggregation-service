package nl.interfixus.aggregation.resource;

import static java.lang.String.format;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import nl.interfixus.aggregation.model.API;

class Call {
    private API api;
    private Set<String> args;
    private Map<String, Object> expected;

    Call(API api, Set<String> args, Map<String, Object> expected) {
        this.api = api;
        this.args = args;
        this.expected = expected;
    }

    public Set<String> getArgs() {
        return args;
    }

    public Map<String, Object> getExpected() {
        return expected;
    }

    public String getArgsAsQueryString() {
        return args.stream().collect(Collectors.joining(","));
    }

    public String getQuery() {
        return format("%s=%s", api, getArgsAsQueryString());
    }
}
