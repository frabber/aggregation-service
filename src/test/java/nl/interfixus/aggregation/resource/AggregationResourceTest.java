package nl.interfixus.aggregation.resource;

import static java.lang.String.format;
import static java.util.List.of;
import static nl.interfixus.aggregation.model.API.pricing;
import static nl.interfixus.aggregation.model.API.shipments;
import static nl.interfixus.aggregation.model.API.track;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.logging.Logger;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.helpers.test.UniAssertSubscriber;
import nl.interfixus.aggregation.client.PricingClient;
import nl.interfixus.aggregation.client.ShipmentClient;
import nl.interfixus.aggregation.client.TrackClient;
import nl.interfixus.aggregation.model.Pair;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AggregationResourceTest extends AggregationResourceBaseTest {

    private static final Logger LOG = Logger.getLogger(AggregationResourceTest.class);

    @InjectMock
    @RestClient
    PricingClient pricingClient;

    @InjectMock
    @RestClient
    TrackClient trackClient;

    @InjectMock
    @RestClient
    ShipmentClient shipmentClient;

    @RestClient
    AggregationClient aggregationClient;

    @Test
    @Order(1)
    void queryTriggersTimeoutAndTwoBackendCalls() {
        performTwoCalls(false);
    }

    @Test
    @Order(2)
    void backendServicUnavailableNullifyAndContinueSecondCall() {
        performTwoCalls(true);
    }

    void performTwoCalls(boolean serviceUnavailable) {
        CallDistribution calls = getCalls(of(4, 4), of(5), of(5));

        if (serviceUnavailable) {
            calls.pricingCalls.get(0).getExpected().entrySet().stream()
                .forEach(entry -> entry.setValue(null));
            when(pricingClient.get(calls.pricingCalls.get(0).getArgs())).thenReturn(
                Uni.createFrom().failure(() -> new RuntimeException()));
        } else {
            when(pricingClient.get(calls.pricingCalls.get(0).getArgs())).thenReturn(
                Uni.createFrom().item(() -> calls.pricingCalls.get(0).getExpected()));
        }
        when(trackClient.get(calls.trackCalls.get(0).getArgs())).thenReturn(
            Uni.createFrom().item(() -> calls.trackCalls.get(0).getExpected()));
        when(shipmentClient.get(calls.shipmentCalls.get(0).getArgs())).thenReturn(
            Uni.createFrom().item(() -> calls.shipmentCalls.get(0).getExpected()));

        when(pricingClient.get(calls.pricingCalls.get(1).getArgs())).thenReturn(
            Uni.createFrom().item(() -> calls.pricingCalls.get(1).getExpected()));

        Map<String, Map<String, Object>> expectedMap1 = getExpectedMap(calls.pricingCalls.get(0),
            calls.trackCalls.get(0),
            calls.shipmentCalls.get(0));

        Map<String, Map<String, Object>> expectedMap2 = getExpectedMap(calls.pricingCalls.get(1),
            null,
            null);

        logCalls(calls, 0, 0, 0);

        UniAssertSubscriber<Map<String, Map<String, Object>>> subscriberCall1 =
            aggregationClient.get(calls.pricingCalls.get(0).getArgsAsQueryString(),
                    calls.trackCalls.get(0).getArgsAsQueryString(),
                    calls.shipmentCalls.get(0).getArgsAsQueryString())
                .subscribe()
                .withSubscriber(UniAssertSubscriber.create());

        logCalls(calls, 1, 0, 0);

        UniAssertSubscriber<Map<String, Map<String, Object>>> subscriberCall2 =
            aggregationClient.get(calls.pricingCalls.get(1).getArgsAsQueryString(),
                    null,
                    "")
                .subscribe()
                .withSubscriber(UniAssertSubscriber.create());

        Pair pair1 = Pair.of(subscriberCall1, expectedMap1);
        Pair pair2 = Pair.of(subscriberCall2, expectedMap2);
        LOG.info(format("call 1 %s\ncall 2 %S", pair1, pair2));
        assertSubscribers(List.of(
            pair1,
            pair2
        ));
    }

    private void logCalls(CallDistribution calls, int priceIndex, int trackIndex, int shipmentIndex) {
        String request = format("/aggregation?%s&%s&%s", calls.pricingCalls.get(priceIndex).getQuery(),
            calls.trackCalls.get(trackIndex).getQuery(),
            calls.shipmentCalls.get(shipmentIndex).getQuery());
        LOG.info(format("\tcurl \"localhost:8080%s\" &", request));
    }

    private CallDistribution getCalls(List<Integer> pricingCallDistribution,
        List<Integer> trackCallDistribution,
        List<Integer> shipmentCallDistribution) {

        CallDistribution callDistribution = new CallDistribution();
        for (Set<String> pricingArgs : getListOfSetOfCountries(pricingCallDistribution)) {
            callDistribution.pricingCalls.add(new Call(pricing, pricingArgs, getMockPricing(pricingArgs)));
        }
        for (Set<String> trackArgs : getListOfSetOfOrderNumbers(trackCallDistribution)) {
            callDistribution.trackCalls.add(new Call(track, trackArgs, getMockTrack(trackArgs)));
        }
        for (Set<String> shipmentArgs : getListOfSetOfOrderNumbers(shipmentCallDistribution)) {
            callDistribution.shipmentCalls.add(new Call(shipments, shipmentArgs, getMockShipment(shipmentArgs)));
        }
        return callDistribution;
    }

    private static class CallDistribution {
        public List<Call> pricingCalls = new ArrayList<>();
        public List<Call> trackCalls = new ArrayList<>();
        public List<Call> shipmentCalls = new ArrayList<>();
    }

}