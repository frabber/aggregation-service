/*
 * Copyright (c) 2022, Faizal Abdoelrahman, Interfixus.
 */

package nl.interfixus.aggregation.resource;

import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import org.eclipse.microprofile.faulttolerance.Retry;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import io.smallrye.mutiny.Uni;

@Path("/aggregation")
@RegisterRestClient(configKey = "aggregation-api")
public interface AggregationClient {

    @GET
    @Retry(maxRetries = 0)
    Uni<Map<String, Map<String, Object>>> get(@QueryParam("pricing") String pricing,
        @QueryParam("track") String track,
        @QueryParam("shipments") String shipments);

}
